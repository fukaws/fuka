\page fukascience Research Enabled by FUKA

## Known scientific works enabled by the FUKA suite of initial data solvers

-# ***Black hole pulsars and monster shocks as outcomes of black hole-neutron star mergers***; Kim, Yoonsoo, Most, Elias R., Beloborodov, Andrei M., Ripperda, Bart; [arxiv:2412.05760](https://arxiv.org/abs//2412.05760); Cites: 0  
-# ***GRoovy: A General Relativistic Hydrodynamics Code for Dynamical Spacetimes with Curvilinear Coordinates, Tabulated Equations of State, and Neutrino Physics***; Jacques, Terrence Pierre, Cupp, Samuel, Werneck, Leonardo R., Tootle, Samuel D., Hamilton, Maria C. Babiuc, Etienne, Zachariah B.; [arxiv:2412.03659](https://arxiv.org/abs//2412.03659); Cites: 0  
-# ***Accurate muonic interactions in neutron-star mergers and impact on heavy-element nucleosynthesis***; Ng, Harry Ho-Yin, Musolino, Carlo, Tootle, Samuel D., Rezzolla, Luciano; [arxiv:2411.19178](https://arxiv.org/abs//2411.19178); Cites: 1  
-# ***Fast dynamic ejecta in neutron star mergers***; Rosswog, Stephan, Sarin, Nikhil, Nakhar, Ehud, Diener, Peter; [arxiv:2411.18813](https://arxiv.org/abs//2411.18813); Cites: 0  
-# ***Tidal Resonance in Binary Neutron Star Inspirals: A High-Precision Study in Numerical Relativity***; Kuan, Hao-Jui, Kiuchi, Kenta, Shibata, Masaru; [arxiv:2411.16850](https://arxiv.org/abs//2411.16850); Cites: 0  
-# ***On the impact of neutrinos on the launching of relativistic jets from "magnetars" produced in neutron-star mergers***; Musolino, Carlo, Rezzolla, Luciano, Most, Elias R.; [arxiv:2410.06253](https://arxiv.org/abs//2410.06253); Cites: 5  
-# ***Problematic systematics in neutron-star merger simulations***; Gittins, F., Matur, R., Andersson, N., Hawke, I.; [arxiv:2409.13468](https://arxiv.org/abs//2409.13468); Cites: 0  
-# ***The Influence of Muons, Pions, and Trapped Neutrinos on Neutron Star Mergers***; Pajkos, Michael A., Most, Elias R.; [arxiv:2409.09147](https://arxiv.org/abs//2409.09147); Cites: 2  
-# ***Black hole - neutron star binaries with high spins and large mass asymmetries: II. Properties of dynamical simulations***; Topolski, Konrad, Tootle, Samuel, Rezzolla, Luciano; [arxiv:2409.06777](https://arxiv.org/abs//2409.06777); Cites: 1  
-# ***Black hole-neutron star binaries with high spins and large mass asymmetries: I. Properties of quasi-equilibrium sequences***; Topolski, Konrad, Tootle, Samuel, Rezzolla, Luciano; [arxiv:2409.06767](https://arxiv.org/abs//2409.06767); Cites: 0  
-# ***General relativistic magnetohydrodynamic simulations with bam: Implementation and code comparison***; Neuweiler, Anna, Dietrich, Tim, Brügmann, Bernd, Giangrandi, Edoardo, Kiuchi, Kenta, Schianchi, Federico, Mösta, Philipp, Shankar, Swapnil, Giacomazzo, Bruno, Shibata, Masaru; [DOI:10.1103/PhysRevD.110.084046](doi.org/10.1103/PhysRevD.110.084046); Cites: 1  
-# ***Signatures of low-mass black hole–neutron star mergers***; Matur, Rahime, Hawke, Ian, Andersson, Nils; [DOI:10.1093/mnras/stae2238](doi.org/10.1093/mnras/stae2238); Cites: 3  
-# ***Nonlinear Alfvén-wave Dynamics and Premerger Emission from Crustal Oscillations in Neutron Star Mergers***; Most, Elias R., Kim, Yoonsoo, Chatziioannou, Katerina, Legred, Isaac; [DOI:10.3847/2041-8213/ad785c](doi.org/10.3847/2041-8213/ad785c); Cites: 3  
-# ***Binary neutron star mergers in massive scalar-tensor theory: Properties of postmerger remnants***; Lam, Alan Tsz-Lok, Kuan, Hao-Jui, Shibata, Masaru, Van Aelst, Karim, Kiuchi, Kenta; [DOI:10.1103/PhysRevD.110.104018](doi.org/10.1103/PhysRevD.110.104018); Cites: 3  
-# ***General-relativistic gauge-invariant magnetic helicity transport: Basic formulation and application to neutron star mergers***; Wu, Jiaxi, Most, Elias R.; [DOI:10.1103/PhysRevD.110.124046](doi.org/10.1103/PhysRevD.110.124046); Cites: 0  
-# ***Black hole-neutron star mergers in Einstein-scalar-Gauss-Bonnet gravity***; Corman, Maxence, East, William E.; [DOI:10.1103/PhysRevD.110.084065](doi.org/10.1103/PhysRevD.110.084065); Cites: 8  
-# ***Effect of spin in binary neutron star mergers***; Karakas, Beyhan, Matur, Rahime, Ruffert, Maximilian; [arxiv:2405.13687](https://arxiv.org/abs//2405.13687); Cites: 1  
-# ***Black hole-neutron star mergers with massive neutron stars in numerical relativity***; Chen, Shichuan, Wang, Luohan, Hayashi, Kota, Kawaguchi, Kyohei, Kiuchi, Kenta, Shibata, Masaru; [DOI:10.1103/PhysRevD.110.063016](doi.org/10.1103/PhysRevD.110.063016); Cites: 7  
-# ***SPHINCS_BSSN: Numerical Relativity with Particles***; Rosswog, S., Diener, P.; [arxiv:2404.15952](https://arxiv.org/abs//2404.15952); Cites: 2  
-# ***Large eddy simulations of magnetized mergers of black holes and neutron stars***; Izquierdo, Manuel R., Bezares, Miguel, Liebling, Steven, Palenzuela, Carlos; [DOI:10.1103/PhysRevD.110.083017](doi.org/10.1103/PhysRevD.110.083017); Cites: 11  
-# ***Listening to the long ringdown: a novel way to pinpoint the equation of state in neutron-star cores***; Ecker, Christian, Gorda, Tyler, Kurkela, Aleksi, Rezzolla, Luciano; [arxiv:2403.03246](https://arxiv.org/abs//2403.03246); Cites: 6  
-# ***Prompt black hole formation in binary neutron star mergers***; Ecker, Christian, Topolski, Konrad, Järvinen, Matti, Stehr, Alina; [DOI:10.1103/PhysRevD.111.023001](doi.org/10.1103/PhysRevD.111.023001); Cites: 7  
-# ***Microphysical Aspects of Binary Neutron Star Mergers***; Chabanov, Michail, Cruz-Osorio, Alejandro, Ecker, Christian, Meringolo, Claudio, Musolino, Carlo, Rezzolla, Luciano, Tootle, Samuel, Topolski, Konrad; [DOI:10.1007/978-3-031-46870-4_2](doi.org/10.1007/978-3-031-46870-4_2); Cites: 0  
-# ***Hybrid approach to long-term binary neutron-star simulations***; Ng, Harry Ho-Yin, Jiang, Jin-Liang, Musolino, Carlo, Ecker, Christian, Tootle, Samuel D., Rezzolla, Luciano; [DOI:10.1103/PhysRevD.109.064061](doi.org/10.1103/PhysRevD.109.064061); Cites: 9  
-# ***Impact of a mean field dynamo on neutron star mergers leading to magnetar remnants***; Most, Elias R.; [DOI:10.1103/PhysRevD.108.123012](doi.org/10.1103/PhysRevD.108.123012); Cites: 15  
-# ***Mergers of double NSs with one high-spin component: brighter kilonovae and fallback accretion, weaker gravitational waves***; Rosswog, S., Diener, P., Torsello, F., Tauris, T.M., Sarin, N.; [DOI:10.1093/mnras/stae454](doi.org/10.1093/mnras/stae454); Cites: 10  
-# ***Post-merger Gravitational-wave Signal from Neutron-star Binaries: A New Look at an Old Problem***; Topolski, Konrad, Tootle, Samuel D., Rezzolla, Luciano; [DOI:10.3847/1538-4357/ad0152](doi.org/10.3847/1538-4357/ad0152); Cites: 13  
-# ***Electromagnetic Precursors to Black Hole–Neutron Star Gravitational Wave Events: Flares and Reconnection-powered Fast Radio Transients from the Late Inspiral***; Most, Elias R., Philippov, Alexander A.; [DOI:10.3847/2041-8213/acfdae](doi.org/10.3847/2041-8213/acfdae); Cites: 16  
-# ***Binary neutron star mergers in massive scalar-tensor theory: Quasiequilibrium states and dynamical enhancement of the scalarization***; Kuan, Hao-Jui, Van Aelst, Karim, Lam, Alan Tsz-Lok, Shibata, Masaru; [DOI:10.1103/PhysRevD.108.064057](doi.org/10.1103/PhysRevD.108.064057); Cites: 10  
-# ***Impact of bulk viscosity on the post-merger gravitational-wave signal from merging neutron stars***; Chabanov, Michail, Rezzolla, Luciano; [arxiv:2307.10464](https://arxiv.org/abs//2307.10464); Cites: 36  
-# ***The Lagrangian Numerical Relativity code SPHINCS_BSSN_v1.0***; Rosswog, Stephan, Torsello, Francesco, Diener, Peter; [DOI:10.3389/fams.2023.1236586](doi.org/10.3389/fams.2023.1236586); Cites: 3  
-# ***General-relativistic hydrodynamics simulation of a neutron star–sub-solar-mass black hole merger***; Markin, Ivan, Neuweiler, Anna, Abac, Adrian, Chaurasia, Swami Vivekanandji, Ujevic, Maximiliano, Bulla, Mattia, Dietrich, Tim; [DOI:10.1103/PhysRevD.108.064025](doi.org/10.1103/PhysRevD.108.064025); Cites: 10  
-# ***Flares, Jets, and Quasiperiodic Outbursts from Neutron Star Merger Remnants***; Most, Elias R., Quataert, Eliot; [DOI:10.3847/2041-8213/acca84](doi.org/10.3847/2041-8213/acca84); Cites: 29  
-# ***Dynamical scalarization during neutron star mergers in scalar-Gauss-Bonnet theory***; Kuan, Hao-Jui, Lam, Alan Tsz-Lok, Doneva, Daniela D., Yazadjiev, Stoytcho S., Shibata, Masaru, Kiuchi, Kenta; [DOI:10.1103/PhysRevD.108.063033](doi.org/10.1103/PhysRevD.108.063033); Cites: 13  
-# ***Crustal Magnetic Fields Do Not Lead to Large Magnetic-field Amplifications in Binary Neutron Star Mergers***; Chabanov, Michail, Tootle, Samuel D., Most, Elias R., Rezzolla, Luciano; [DOI:10.3847/2041-8213/acbbc5](doi.org/10.3847/2041-8213/acbbc5); Cites: 20  
-# ***Quark formation and phenomenology in binary neutron-star mergers using V-QCD***; Tootle, Samuel, Ecker, Christian, Topolski, Konrad, Demircik, Tuna, Järvinen, Matti, Rezzolla, Luciano; [DOI:10.21468/SciPostPhys.13.5.109](doi.org/10.21468/SciPostPhys.13.5.109); Cites: 45  
-# ***Impact of extreme spins and mass ratios on the post-merger observables of high-mass binary neutron stars***; Papenfort, L. Jens, Most, Elias R., Tootle, Samuel, Rezzolla, Luciano; [DOI:10.1093/mnras/stac964](doi.org/10.1093/mnras/stac964); Cites: 30  
-# ***Quasi-universal Behavior of the Threshold Mass in Unequal-mass, Spinning Binary Neutron Star Mergers***; Tootle, Samuel D., Papenfort, L. Jens, Most, Elias R., Rezzolla, Luciano; [DOI:10.3847/2041-8213/ac350d](doi.org/10.3847/2041-8213/ac350d); Cites: 42  
-# ***On accretion discs formed in MHD simulations of black hole–neutron star mergers with accurate microphysics***; Most, Elias R., Papenfort, L. Jens, Tootle, Samuel D., Rezzolla, Luciano; [DOI:10.1093/mnras/stab1824](doi.org/10.1093/mnras/stab1824); Cites: 36  
-# ***Fast ejecta as a potential way to distinguish black holes from neutron stars in high-mass gravitational-wave events***; Most, Elias R., Papenfort, L. Jens, Tootle, Samuel, Rezzolla, Luciano; [DOI:10.3847/1538-4357/abf0a5](doi.org/10.3847/1538-4357/abf0a5); Cites: 31  


### Citation Statistics


Average citations of FUKA enabled publications: 11  
Maximum citations of FUKA enabled publications: 45  

This page has been generated using the [Inspire REST API](https://github.com/inspirehep/rest-api-doc)  
Last updated: 2025-01-06